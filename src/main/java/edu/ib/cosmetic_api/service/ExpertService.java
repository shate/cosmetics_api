package edu.ib.cosmetic_api.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;
import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.repository.CosmeticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class ExpertService {
    private CosmeticRepository cosmeticRepository;
    Firestore db = FirestoreClient.getFirestore();


    @Autowired
    public ExpertService(CosmeticRepository cosmeticRepository) {
        this.cosmeticRepository = cosmeticRepository;
    }

    public List<Cosmetic> getUncheckedCosmetics() throws ExecutionException, InterruptedException {
        List<Cosmetic> allCosmetics = cosmeticRepository.findAll();
        List<Cosmetic> uncheckedCosmetics = new ArrayList<>();
        for(Cosmetic cosmetic : allCosmetics){
            if(!cosmetic.isConfirmed()){
                uncheckedCosmetics.add(cosmetic);
            }
        }
        return uncheckedCosmetics;
    }
}
