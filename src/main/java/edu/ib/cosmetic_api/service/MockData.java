package edu.ib.cosmetic_api.service;

import edu.ib.cosmetic_api.persistance.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
public class MockData {
    private UserService userService;


    @Autowired
    public MockData(UserService userService) {
        this.userService = userService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() throws ExecutionException, InterruptedException {
        String roleUser = "ROLE_USER";
        String roleAdmin = "ROLE_ADMIN";
        String email = "user@user";
        String email1 = "admin@admin";
        String password = "user";
        String password1 = "admin";
        User user = new User(email, password, Arrays.asList(roleUser), true);
        User admin = new User(email1, password1, Arrays.asList(roleAdmin), true);
        userService.saveUser(user);
        userService.saveUser(admin);
    }

}
