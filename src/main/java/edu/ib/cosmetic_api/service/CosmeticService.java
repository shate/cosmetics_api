package edu.ib.cosmetic_api.service;

import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.repository.CosmeticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class CosmeticService {
    private CosmeticRepository cosmeticRepository;

    @Autowired
    public CosmeticService(CosmeticRepository cosmeticRepository) {
        this.cosmeticRepository = cosmeticRepository;
    }

    public List<Cosmetic> findAll() throws ExecutionException, InterruptedException {
        return cosmeticRepository.findAll();
    }

    public Cosmetic findByName(String name) throws ExecutionException, InterruptedException {
        return cosmeticRepository.findByName(name);
    }

    public Cosmetic findById(Long id) throws ExecutionException, InterruptedException {
        return cosmeticRepository.findById(id);
    }

    public List<Cosmetic> findByProducer(String producer) throws ExecutionException, InterruptedException {
        return cosmeticRepository.findByProducer(producer);
    }

    public List<Cosmetic> findByPurpose(String purpose) throws ExecutionException, InterruptedException {
        return cosmeticRepository.findByPurpose(purpose);
    }

    public void update(Cosmetic cosmetic){
        cosmeticRepository.update(cosmetic);
    }

    public List<String> getPurposes() throws ExecutionException, InterruptedException {
        return cosmeticRepository.getPurposes();
    }

    public List<String> getProducers() throws ExecutionException, InterruptedException {
        return cosmeticRepository.getProducers();
    }
}
