package edu.ib.cosmetic_api.service;

import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.entity.Ingredient;
import edu.ib.cosmetic_api.persistance.repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class IngredientService {

    private IngredientRepository ingredientRepository;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public List<Ingredient> findAll() throws ExecutionException, InterruptedException {
        return ingredientRepository.findAll();
    }

    public Ingredient findByName(String name) throws ExecutionException, InterruptedException {
        return ingredientRepository.findByName(name);
    }

    public List<Ingredient> findByHarmfulness(String harmfulness) throws ExecutionException, InterruptedException {
        return ingredientRepository.findByHarmfulness(harmfulness);
    }

    public void addIngredient (Ingredient ingredient) throws ExecutionException, InterruptedException {
         ingredientRepository.addToDb(ingredient);
    }
}
