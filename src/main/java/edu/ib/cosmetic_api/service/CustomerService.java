package edu.ib.cosmetic_api.service;

import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.entity.Customer;
import edu.ib.cosmetic_api.persistance.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer findByEmail(String email) throws ExecutionException, InterruptedException {
        return customerRepository.findByEmail(email);
    }

    public List<Cosmetic> getCosmetics(String email) throws ExecutionException, InterruptedException {
        return customerRepository.getCosmetics(email);
    }
}
