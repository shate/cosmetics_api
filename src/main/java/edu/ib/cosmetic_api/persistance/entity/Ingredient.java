package edu.ib.cosmetic_api.persistance.entity;

public class Ingredient {
    private String name;
    private String detail;
    private String harmfulness;

    public Ingredient() {
    }

    public Ingredient(String name, String detail, String harmfulness) {
        this.name = name;
        this.detail = detail;
        this.harmfulness = harmfulness;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getHarmfulness() {
        return harmfulness;
    }

    public void setHarmfulness(String harmfulness) {
        this.harmfulness = harmfulness;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "name='" + name + '\'' +
                ", detail='" + detail + '\'' +
                ", harmfulness='" + harmfulness + '\'' +
                '}';
    }
}
