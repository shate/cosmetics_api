package edu.ib.cosmetic_api.persistance.entity;


import java.io.Serializable;
import java.util.List;

public class Cosmetic implements Serializable {
    private Long id;
    private String howToUse;
    private String name;
    private String producer;
    private List<String> purpose;
    private List<Ingredient> ingredients;
    private boolean confirmed;

    public Cosmetic() {
    }

    public Cosmetic(Long id, String howToUse, String name, String producer, List<String> purpose, List<Ingredient> ingredients) {
        this.id = id;
        this.howToUse = howToUse;
        this.name = name;
        this.producer = producer;
        this.purpose = purpose;
        this.ingredients = ingredients;
        this.confirmed = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHowToUse() {
        return howToUse;
    }

    public void setHowToUse(String howToUse) {
        this.howToUse = howToUse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public List<String> getPurpose() {
        return purpose;
    }

    public void setPurpose(List<String> purpose) {
        this.purpose = purpose;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    @Override
    public String toString() {
        return "Cosmetic{" +
                "id=" + id +
                ", howToUse='" + howToUse + '\'' +
                ", name='" + name + '\'' +
                ", producer='" + producer + '\'' +
                ", purpose=" + purpose +
                ", ingredients=" + ingredients +
                ", confirmed=" + confirmed +
                '}';
    }
}
