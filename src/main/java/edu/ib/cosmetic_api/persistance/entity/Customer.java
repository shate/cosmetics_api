package edu.ib.cosmetic_api.persistance.entity;

import java.util.List;

public class Customer {
    private String email;
    private List<Cosmetic> cosmetics;
    private int score;

    public Customer() {
    }

    public Customer(String email, List<Cosmetic> cosmetics, int score) {
        this.email = email;
        this.cosmetics = cosmetics;
        this.score = score;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Cosmetic> getCosmetics() {
        return cosmetics;
    }

    public void setCosmetics(List<Cosmetic> cosmetics) {
        this.cosmetics = cosmetics;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "email='" + email + '\'' +
                ", cosmetics=" + cosmetics +
                ", score=" + score +
                '}';
    }
}
