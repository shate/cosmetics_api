package edu.ib.cosmetic_api.persistance.entity;

import java.util.List;

public class User {

    private String email;
    private String password;
    private boolean active;
    private List<String> roles;

    public User() {
    }

    public User(String email, String password, boolean active){
        this.email = email;
        this.password = password;
        this.active = active;
    }

    public User(String email, String password, List<String> roles, boolean active){
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String passwordHash) {
        this.password = passwordHash;
    }

    @Override
    public String toString() {
        return "User{" +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", active=" + active +
                ", roles=" + roles +
                '}';
    }
}
