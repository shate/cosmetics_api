package edu.ib.cosmetic_api.persistance.repository;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.entity.Ingredient;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Repository
public class CosmeticRepository {
    Firestore db = FirestoreClient.getFirestore();

    public List<Cosmetic> findAll() throws ExecutionException, InterruptedException {
        List<Cosmetic> cosmetics = new ArrayList<>();
        ApiFuture<QuerySnapshot> apiFuture = db.collection("cosmetics").get();

        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for(DocumentSnapshot document : documents){
            cosmetics.add(document.toObject(Cosmetic.class));
        }
        return cosmetics;
    }

    public Cosmetic findById(Long id) throws ExecutionException, InterruptedException {
        DocumentReference documentReference = db.collection("cosmetics").document(id.toString());
        Cosmetic cosmetic = documentReference.get().get().toObject(Cosmetic.class);
        return cosmetic;
    }

    public List<Cosmetic> findByProducer(String producer) throws ExecutionException, InterruptedException {
        List<Cosmetic> cosmetics = new ArrayList<>();
        ApiFuture<QuerySnapshot> apiFuture = db.collection("cosmetics").get();

        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for(DocumentSnapshot document : documents){
            Cosmetic cosmetic = document.toObject(Cosmetic.class);
            if(cosmetic.getProducer().equals(producer)) {
                cosmetics.add(cosmetic);
            }
        }
        return cosmetics;
    }

    public Cosmetic findByName(String name) throws ExecutionException, InterruptedException {
        ApiFuture<QuerySnapshot> apiFuture = db.collection("cosmetics").get();

        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for(DocumentSnapshot document : documents){
            Cosmetic cosmetic = document.toObject(Cosmetic.class);
            System.out.println(cosmetic.toString());
            if(cosmetic.getName().equals(name)) {
                return cosmetic;
            }
        }
        return null;
    }

    public List<Cosmetic> findByPurpose(String purpose) throws ExecutionException, InterruptedException {
        List<Cosmetic> cosmetics = new ArrayList<>();
        ApiFuture<QuerySnapshot> apiFuture = db.collection("cosmetics").get();

        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for(DocumentSnapshot document : documents){
            Cosmetic cosmetic = document.toObject(Cosmetic.class);
            if(cosmetic != null && cosmetic.getPurpose() != null) {
                if (cosmetic.getPurpose().contains(purpose)) {
                    cosmetics.add(cosmetic);
                }
            }
        }
        return cosmetics;
    }

    public void update(Cosmetic cosmetic){
        db.collection("cosmetics").document(cosmetic.getId().toString()).set(cosmetic);
    }

    public List<String> getPurposes() throws ExecutionException, InterruptedException {
        List<String> purposes = new ArrayList<>();
        ApiFuture<QuerySnapshot> apiFuture = db.collection("cosmetics").get();

        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for(DocumentSnapshot document : documents){
            Cosmetic cosmetic = document.toObject(Cosmetic.class);
            if(cosmetic != null && cosmetic.getPurpose() != null) {
                for(String purpose : cosmetic.getPurpose()) {
                    if (!purposes.contains(purpose)){
                        purposes.add(purpose);
                    }
                }
            }
        }
        return purposes;
    }

    public List<String> getProducers() throws ExecutionException, InterruptedException {
        List<String> producers = new ArrayList<>();
        ApiFuture<QuerySnapshot> apiFuture = db.collection("cosmetics").get();

        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for(DocumentSnapshot document : documents){
            Cosmetic cosmetic = document.toObject(Cosmetic.class);
            if(cosmetic != null && cosmetic.getProducer() != null) {
                if (!producers.contains(cosmetic.getProducer())){
                    producers.add(cosmetic.getProducer());
                }
            }
        }
        return producers;
    }
}
