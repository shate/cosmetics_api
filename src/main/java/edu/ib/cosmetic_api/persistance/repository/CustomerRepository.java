package edu.ib.cosmetic_api.persistance.repository;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.entity.Customer;
import edu.ib.cosmetic_api.persistance.entity.Ingredient;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Repository
public class CustomerRepository {
    Firestore db = FirestoreClient.getFirestore();

    public Customer findByEmail(String email) throws ExecutionException, InterruptedException {
        DocumentReference documentReference = db.collection("customers").document(email);
        Customer customer = documentReference.get().get().toObject(Customer.class);
        return customer;
    }

    public List<Cosmetic> getCosmetics(String email) throws ExecutionException, InterruptedException {
        ApiFuture<QuerySnapshot> apiFuture = db.collection("customers").document(email).collection("cosmetics").get();
        List<Cosmetic> cosmetics = new ArrayList<>();
        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for (DocumentSnapshot document : documents) {
            Cosmetic cosmetic = document.toObject(Cosmetic.class);
            cosmetics.add(cosmetic);
        }
        return cosmetics;
    }
}
