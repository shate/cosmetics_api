package edu.ib.cosmetic_api.persistance.repository;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.database.DatabaseReference;
import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.entity.Ingredient;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Repository
public class IngredientRepository {
    Firestore db = FirestoreClient.getFirestore();

    public List<Ingredient> findAll() throws ExecutionException, InterruptedException {
        List<Ingredient> ingredients = new ArrayList<>();
        ApiFuture<QuerySnapshot> apiFuture = db.collection("ingredients").get();

        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for (DocumentSnapshot document : documents) {
            ingredients.add(document.toObject(Ingredient.class));
        }
        return ingredients;
    }

    public Ingredient findByName(String name) throws ExecutionException, InterruptedException {
        DocumentReference documentReference = db.collection("ingredients").document(name);
        Ingredient ingredient = documentReference.get().get().toObject(Ingredient.class);
        return ingredient;
    }

    public List<Ingredient> findByHarmfulness(String harmfulness) throws ExecutionException, InterruptedException {
        List<Ingredient> ingredients = new ArrayList<>();
        ApiFuture<QuerySnapshot> apiFuture = db.collection("ingredients").get();

        List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();

        for (DocumentSnapshot document : documents) {
            Ingredient ingredient = document.toObject(Ingredient.class);
            if (ingredient.getHarmfulness().equals(harmfulness)) {
                ingredients.add(ingredient);
            }
        }
        return ingredients;
    }

    public void addToDb(Ingredient ingredient) throws ExecutionException, InterruptedException {
        Map<String,String> ingredientToAdd = new HashMap<>();
        ingredientToAdd.put("name",ingredient.getName());
        ingredientToAdd.put("detail",ingredient.getDetail());
        ingredientToAdd.put("harmfulness",ingredient.getHarmfulness());
        ApiFuture<WriteResult> future = db.collection("ingredients").document(ingredient.getName()).set(ingredientToAdd);

    }
}
