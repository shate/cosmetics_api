package edu.ib.cosmetic_api.persistance.repository;

import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;
import edu.ib.cosmetic_api.persistance.entity.User;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ExecutionException;

@Repository
public class UserRepository {
    Firestore db = FirestoreClient.getFirestore();

    public User findByEmail(String email) throws ExecutionException, InterruptedException {
        DocumentReference documentReference = db.collection("users").document(email);
        User user = documentReference.get().get().toObject(User.class);
        return user;
    }

    public User save(User user) {
        db.collection("users").document(user.getEmail()).set(user);
        return user;
    }
}
