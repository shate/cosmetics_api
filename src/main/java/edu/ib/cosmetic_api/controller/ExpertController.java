package edu.ib.cosmetic_api.controller;

import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.entity.Ingredient;
import edu.ib.cosmetic_api.service.CosmeticService;
import edu.ib.cosmetic_api.service.ExpertService;
import edu.ib.cosmetic_api.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Controller
public class ExpertController {
    private ExpertService expertService;
    private CosmeticService cosmeticService;
    private IngredientService ingredientService;

    @Autowired
    public ExpertController(ExpertService expertService, CosmeticService cosmeticService, IngredientService ingredientService) {
        this.expertService = expertService;
        this.cosmeticService = cosmeticService;
        this.ingredientService = ingredientService;
    }

    @GetMapping("/confirmCosmetics")
    public String confirmCosmetics(Model model) throws ExecutionException, InterruptedException {
        List<Cosmetic> uncheckedCosmetics = expertService.getUncheckedCosmetics();
        System.out.println(uncheckedCosmetics.toString());
        model.addAttribute("uncheckedCosmetics", uncheckedCosmetics);
        return "uncheckedCosmetics";
    }

    @GetMapping("/addIngredient")
    public String addIngredient() throws ExecutionException, InterruptedException {
        return "addIngredient";
    }

    @PostMapping("/addIngredient")
    public String processForm(@RequestParam String name, String detail, String harmfulness) throws ExecutionException, InterruptedException {
        Ingredient ingredient = new Ingredient(name, detail,harmfulness);
        ingredientService.addIngredient(ingredient);
        return "index";
    }

    @PostMapping("confirm")
    public String confirm(@RequestParam Long id, Model model) throws ExecutionException, InterruptedException {
        Cosmetic cosmetic = cosmeticService.findById(id);
        cosmetic.setConfirmed(true);
        cosmeticService.update(cosmetic);
        List<Cosmetic> uncheckedCosmetics = expertService.getUncheckedCosmetics();
        System.out.println(uncheckedCosmetics.toString());
        model.addAttribute("uncheckedCosmetics", uncheckedCosmetics);
        return "uncheckedCosmetics";
    }
}
