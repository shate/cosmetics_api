package edu.ib.cosmetic_api.controller;

import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.service.CosmeticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api")
public class CosmeticController {
    private CosmeticService cosmeticService;

    @Autowired
    public CosmeticController(CosmeticService cosmeticService) {
        this.cosmeticService = cosmeticService;
    }

    @GetMapping("/cosmetic/all")
    public List<Cosmetic> getAll() throws ExecutionException, InterruptedException {
        return cosmeticService.findAll();
    }

    @GetMapping(path = "/cosmetic", params = "name")
    public Cosmetic findByName(@RequestParam(value = "name") String name) throws ExecutionException, InterruptedException {
        return cosmeticService.findByName(name);
    }

    @GetMapping(path = "/cosmetic", params = "id")
    public Cosmetic findById(@RequestParam(value = "id") Long id) throws ExecutionException, InterruptedException {
        return cosmeticService.findById(id);
    }

    @GetMapping(path = "/cosmetic", params = "producer")
    public List<Cosmetic> findByProducer(@RequestParam(value = "producer") String producer) throws ExecutionException, InterruptedException {
        return cosmeticService.findByProducer(producer);
    }

    @GetMapping(path = "/cosmetic", params = "purpose")
    public List<Cosmetic> findByPurpose(@RequestParam(value = "purpose") String purpose) throws ExecutionException, InterruptedException {
        return cosmeticService.findByPurpose(purpose);
    }

}
