package edu.ib.cosmetic_api.controller;

import edu.ib.cosmetic_api.service.CosmeticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Controller
public class ApiController {
    private CosmeticService cosmeticService;

    @Autowired
    public ApiController(CosmeticService cosmeticService) {
        this.cosmeticService = cosmeticService;
    }


    @GetMapping("/api")
    public String getApi(Model model) throws ExecutionException, InterruptedException {
        List<String> purposes = cosmeticService.getPurposes();
        List<String> producers = cosmeticService.getProducers();
        model.addAttribute("purposes", purposes);
        model.addAttribute("producers", producers);
        return "api";
    }
}
