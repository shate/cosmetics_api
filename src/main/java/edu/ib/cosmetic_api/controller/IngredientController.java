package edu.ib.cosmetic_api.controller;

import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.entity.Ingredient;
import edu.ib.cosmetic_api.service.CosmeticService;
import edu.ib.cosmetic_api.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api")
public class IngredientController {
    private IngredientService ingredientService;

    @Autowired
    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @GetMapping("/ingredient/all")
    public List<Ingredient> findAll() throws ExecutionException, InterruptedException {
        return ingredientService.findAll();
    }

    @GetMapping(path = "/ingredient", params = "name")
    public Ingredient findByName(@RequestParam String name) throws ExecutionException, InterruptedException {
        return ingredientService.findByName(name);
    }

    @GetMapping(path = "/ingredient", params = "harmfulness")
    public List<Ingredient> findByHarmfulness(@RequestParam String harmfulness) throws ExecutionException, InterruptedException {
        return ingredientService.findByHarmfulness(harmfulness);
    }
}
