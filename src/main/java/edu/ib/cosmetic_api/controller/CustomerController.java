package edu.ib.cosmetic_api.controller;

import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.entity.Customer;
import edu.ib.cosmetic_api.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Controller
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/myCosmetics")
    public String myCosmetics(Model model) throws ExecutionException, InterruptedException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<Cosmetic> cosmetics = customerService.getCosmetics(auth.getName());
        System.out.println(cosmetics.toString());
        model.addAttribute("cosmetics", cosmetics);
        return "myCosmetics";
    }
}
