package edu.ib.cosmetic_api.controller;

import edu.ib.cosmetic_api.persistance.entity.Cosmetic;
import edu.ib.cosmetic_api.persistance.entity.Ingredient;
import edu.ib.cosmetic_api.service.CosmeticService;
import edu.ib.cosmetic_api.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Controller
public class ClientController {

    private CosmeticService cosmeticService;
    private IngredientService ingredientService;

    @Autowired
    public ClientController(CosmeticService cosmeticService, IngredientService ingredientService) {
        this.cosmeticService = cosmeticService;
        this.ingredientService = ingredientService;
    }

    @GetMapping("/allCosmetics")
    public String getAllCosmetics(Model model) throws ExecutionException, InterruptedException {
        List<Cosmetic> allCosmetics = cosmeticService.findAll();
        model.addAttribute("cosmetics", allCosmetics);
        return "allCosmetics";
    }

    @GetMapping("/allIngredients")
    public String getAllIngredients(Model model) throws ExecutionException, InterruptedException {
        List<Ingredient> allIngredients = ingredientService.findAll();
        model.addAttribute("ingredients", allIngredients);
        return "allIngredients";
    }

    @PostMapping("/cosmetic")
    public String getCosmetic(@RequestParam Long id, Model model) throws ExecutionException, InterruptedException {
        Cosmetic cosmetic = cosmeticService.findById(id);
        model.addAttribute("cosmetic", cosmetic);
        return "cosmetic";
    }

    @PostMapping("/ingredient")
    public String getIngredient(@RequestParam String name, Model model) throws ExecutionException, InterruptedException {
        Ingredient ingredient = ingredientService.findByName(name);
        model.addAttribute("ingredient", ingredient);
        return "ingredient";
    }

    @GetMapping("/home")
    public String home(){
        return "index";
    }

    @GetMapping("/account")
    public String account(Model model){

        return "account";
    }



}
