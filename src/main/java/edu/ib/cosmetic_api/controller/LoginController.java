package edu.ib.cosmetic_api.controller;

import edu.ib.cosmetic_api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

//import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class LoginController {

    private UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public ModelAndView login(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    /*
    @GetMapping("/registration")
    public String registration(Model model){

        Customer customer = new Customer();
        model.addAttribute("customer", customer);
        return "registration";
    }

    @PostMapping("/registration")
    public String createNewUser(@Valid Customer customer, BindingResult bindingResult, Model model) {
        User user = getUserFromCustomer(customer);
        User userExists = userService.findByUsername(user.getUsername());
        if (userExists != null) {
            bindingResult
                    .rejectValue("username", "error.user",
                            "There is already a user registered with the user name provided");
        }
        if (bindingResult.hasErrors()) {
            return "registration";
        } else {
            List<Visit> visits = new ArrayList<>();
            List<Medicine> medicines = new ArrayList<>();
            List<Disease> diseases = new ArrayList<>();
            MedicalHistory medicalHistory = new MedicalHistory(visits, medicines, diseases);
            medicalHistoryService.save(medicalHistory);
            customer.setMedicalHistory(medicalHistory);
            customerService.saveCustomer(customer);
            userService.saveUser(user);
            model.addAttribute("successMessage", "User has been registered successfully");
            model.addAttribute("user", new User());
            return "index";
        }
    }

    private User getUserFromCustomer(Customer customer) {
        return new User(customer.getUsername(), customer.getPassword(), true);
    }

     */
}